﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILogic : MonoBehaviour
{
    public Text timeText;
    public Text pMText;
    public GameObject successText;
    public GameObject PMText;
    public float time;
    private void Update()
    {
        timeText.text = "Time: " + (time -=Time.deltaTime);
        if(time <= 0)
        {
            timeText.text = "Time's up";
            PMText.SetActive(true);
            pMText.text = "Insufficient time for Extraction";
        }
    }

}

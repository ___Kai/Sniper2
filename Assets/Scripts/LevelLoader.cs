using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public GameObject loadingScreen;
    public GameObject dialogueScreen;
    public Slider slider;
    public TextMeshProUGUI textField;
    public CanvasGroup alphaCanvas;
    public string[] tips;
    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsyncronously(sceneIndex));
        StartCoroutine(GenerateTips());
        dialogueScreen.SetActive(false);
    }
    IEnumerator LoadAsyncronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingScreen.SetActive(true);
        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            yield return null;
        }
    }
    public int tipCount;
    IEnumerator GenerateTips()
    {
        tipCount = Random.Range(0, tips.Length);
        textField.text = tips[tipCount];
        while(loadingScreen.activeInHierarchy)
        {
            yield return new WaitForSeconds(3f);
            alphaCanvas.alpha = Mathf.Lerp(0f, 1f, 0.08f);
            yield return new WaitForSeconds(0.5f);
            tipCount++;
            if(tipCount>tips.Length)
            {
                tipCount = 0;
            }
            textField.text = tips[tipCount];
            alphaCanvas.alpha = Mathf.Lerp(0f, 1f, 0.08f);

        }
    }
}

using UnityEngine;

public class Gun : MonoBehaviour
{
    public float damage;
    public float range;
    public float impactForce;
    public float fireRate;
    public Camera fpsCam;
    public ParticleSystem particleSystem1;
    public GameObject impactEffect;
    private float nextTimeToFire;
    void Update()
    {
        if(Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
    }
    void Shoot()
    {
        particleSystem1.Play();
        RaycastHit Hitinfo;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out Hitinfo, range))
        {
            Debug.Log(Hitinfo.transform.name);
            Enemy target = Hitinfo.transform.GetComponent<Enemy>(); 
            if(target != null)
            {
                target.takeDamage(damage);

            }
            if(Hitinfo.rigidbody != null)
            {
                Hitinfo.rigidbody.AddForce(-Hitinfo.normal * impactForce);
            }
            GameObject impactGO = Instantiate(impactEffect, Hitinfo.point, Quaternion.LookRotation(Hitinfo.normal));
            Destroy(impactGO, 2f);
        }
    }
}
